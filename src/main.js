require("./plugins/alert")

import Vue from "vue"
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "@firebase/messaging";
import CKEditor from '@ckeditor/ckeditor5-vue';
import "./plugins/vuetify"
import router from "./router/"
import store from "./store/index.js"
import "./registerServiceWorker"
import "roboto-fontface/css/roboto/roboto-fontface.css"
import "font-awesome/css/font-awesome.css"
import axios from "axios"
import VueAxios from "vue-axios"
import VueAuth from "@websanova/vue-auth"
import auth from "./auth"
import VeeValidate from "vee-validate"
import App from "./views/App.vue"
import 'vue2-timepicker/dist/VueTimepicker.css'
import Url from "@/api/local"
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

import FullCalendar from "vue-full-calendar";
import "fullcalendar/dist/fullcalendar.min.css";

Vue.use(FullCalendar);

//=========================format value input=========================
import money from 'v-money'

Vue.use(money, {precision: 0, decimal: ',', thousands: '.', prefix: '', suffix: '', masked: false});


Vue.mixin({
    methods:{
        unMaskedNumber(value) {
            return value.replace(/\./g, "");
        }
    }
});

//format money
Vue.filter('formatPrice', function (value) {
    let val = (value / 1).toFixed(0).replace('.', ',')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
})

//format date dd-mm-yyyy
Vue.filter('formatDate', function (value) {
    var date = new Date(value);
    if (!isNaN(date.getTime())) {
        var day = date.getDate().toString();
        var month = (date.getMonth() + 1).toString();
        // Months use 0 index.

        return (day[1] ? day : '0' + day[0]) + '-' +
            (month[1] ? month : '0' + month[0]) + '-' +
            date.getFullYear();
    }
})
//===================notification config=============================
import Notifications from 'vue-notification';
Vue.use(Notifications)
var config = {
    apiKey: "AIzaSyDXItL04TZaz8XXkznUjMwDwjtHgTtFFOk",
    authDomain: "logistic-dc8a9.firebaseapp.com",
    databaseURL: "https://logistic-dc8a9.firebaseio.com",
    projectId: "logistic-dc8a9",
    storageBucket: "",
    messagingSenderId: "269017212389",
    appId: "1:269017212389:web:0cea8393d23b5f5c"
};

firebase.initializeApp(config);
Vue.prototype.messaging = firebase.messaging();
Vue.prototype.messaging.usePublicVapidKey("BPt82RXRRbhpHKi7bLiaIrzdNWfRr77gu6uiDxl5WgXKlaQY9oziBSsq5sU9iVyTbaEj1cT581wJzGVnNR9yOpY");
//==================================================================

Vue.use(require('vue-shortkey'));
window.axios = require("axios");
Vue.use(VueAxios, axios);
Vue.config.productionTip = false;
Vue.use(VeeValidate);
Vue.use(Loading);
Vue.use(CKEditor);

//==============calendar=============================================
import ACalendar from './views/order_job/Listorderjob.vue'
export default {
    install: function (Vue, options) {
        Vue.component('full-calendar', ACalendar);
    },
}
export {ACalendar}

//======================================================================

axios.defaults.baseURL = Url.url;
Vue.router = router
Vue.use(VueAuth, auth)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app")
