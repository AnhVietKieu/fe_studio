
import * as actions from "./action";

const store = {
  state: {
    listExpense: null
  },
  getters: {
    listExpense: state => state.listExpense
  },
  mutations: {
    GET_LIST_EXPENSE: (state, payload) => {
      state.listExpense = payload.data.success;
    }
  },
  actions
};
export default store;