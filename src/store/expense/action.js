import Url from "@/api/local"

export const getPaginativeExpense = ({commit}, page) => {
  var lis = Url.url+'/api/expense?page=' + page;
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const getListExpense = ({commit}) => {
  var lis = Url.url+'/api/expense/list/select';
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        commit('GET_LIST_EXPENSE', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const createEx = ({commit}, opts) => {
  var cr = Url.url+'/api/expense/create';
  return new Promise((resolve, reject) => {
    axios.post(cr, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const updateExpense = ({commit}, opts) => {
  var edi = Url.url+'/api/expense/' + opts.id + '/update';
  return new Promise((resolve, reject) => {
    axios.post(edi, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const deleteExpense = ({commit}, opts) => {
  var del = Url.url+'/api/expense/delete';
  return new Promise((resolve, reject) => {
    axios.post(del, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const editExpense = ({commit}, opts) => {
  // console.log(opts);
  var edit = Url.url+'/api/expense/';
  return new Promise((resolve, reject) => {
    axios.get(edit + opts)
      .then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}
