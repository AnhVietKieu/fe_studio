import Url from "@/api/local"

export const getPaginativeSupplier = ({commit}, page, search) => {
    var lis = Url.url+'/api/supplier?page=' + page  + '&search=' + search;
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getSupplier = ({commit}, opts) => {
var vie = Url.url+'/api/supplier/';
    return new Promise((resolve, reject) => {
        axios.get(vie+ opts)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
}
var cre = Url.url+'/api/supplier/create';
export const createSupplier = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios.post(cre, {
            ...opts
        }).then(response => {
            console.log(cre);
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}
export const updateSupplier = ({commit}, opts) => {
var edi = Url.url+'/api/supplier/' + opts.id + '/update';
    return new Promise((resolve, reject) => {
        axios.post(edi, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const deleteSupplier = ({commit}, opts) => {
    var del = Url.url+'/api/supplier/delete';
    return new Promise((resolve, reject) => {
        axios.post(del, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const getListSupplier = ({commit}) => {
  var lis_select = Url.url+'/api/supplier/list/select';
  return new Promise((resolve, reject) => {
    axios.get(lis_select)
      .then(response => {
        commit('GET_LIST_SUPPLIERS', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const listsAdvancedSupplier = ({commit}, opts) => {
    var advanced = Url.url+'/api/supplier/list/search?page=' + opts.page;
    return new Promise((resolve, reject) => {
        axios.post(advanced, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const getHistoryJobSupplier = ({commit}, supplierId) => {
    var listJobSupplier = Url.url+'/api/supplier/' + supplierId + '/job';
    return new Promise((resolve, reject) => {
        axios.get(listJobSupplier)
        .then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}