import * as actions from "./action";

const store = {
    state: {
        listSupplier: null
    },
    getters: {
      listSupplier: state => state.listSupplier
    },
    mutations: {
        GET_LIST_SUPPLIERS: (state, payload) => {
            state.listSupplier = payload.data.success;
        }
    },
    actions
};
export default store;
