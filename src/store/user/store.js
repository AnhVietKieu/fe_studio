import * as actions from "./action";

const store = {
    state: {
        listUser: null,
        listPhotoshoper: null,
        listPhotographer: null,
        listSaler: null,
    },
    getters: {
        listUser: state => state.listUser,
        listPhotoshoper: state => state.listPhotoshoper,
        listPhotographer: state => state.listPhotographer,
        listSaler: state => state.listSaler
    },
    mutations: {
        GET_LIST_USER: (state, payload) => {
            state.listUser = payload.data.success;
        },
        GET_LIST_PHOTOSHOPER: (state, payload) => {
            state.listPhotoshoper = payload.data.success;
        },
        GET_LIST_PHOTOGRAPHER: (state, payload) => {
            state.listPhotographer = payload.data.success;
        }
        ,
        GET_LIST_SALER: (state, payload) => {
            state.listSaler = payload.data.success;
        }
    },
    actions
};

export default store;





