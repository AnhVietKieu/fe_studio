import Url from "@/api/local"


export const getPaginativeUser = ({commit}, page) => {
  var lis = Url.url + '/api/user?page=' + page;
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const LockUser = ({commit}, opts) => {
  var edi = Url.url + '/api/user/' + opts + '/lock';
  return new Promise((resolve, reject) => {
    axios.post(edi, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const Reset = ({commit}, opts) => {
  var edi = Url.url + '/api/auth/reset';
  return new Promise((resolve, reject) => {
    axios.post(edi, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

var cre = Url.url + '/api/auth/register';
export const Register = ({commit}, opts) => {
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const getListUser = ({commit}, type) => {
  var lis_user = Url.url + '/api/user/list/select?type=' + type;
  return new Promise((resolve, reject) => {
    axios.get(lis_user)
      .then(response => {
        commit('GET_LIST_USER', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const getDetail = ({commit}, opts) => {
  var user = Url.url + '/api/user/' + opts+'/view';
  return new Promise((resolve, reject) => {
    axios.get(user)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}


export const editUser = ({commit}, opts) => {
  var cre = Url.url + '/api/user/'+opts.id +'/edit';
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}
export const getListSaler = ({commit}) => {
    var lis_user = Url.url+'/api/user/list/select?department_id=2&type=1';
    // var lis_user = Url.url+'/api/user/list?department_id=2';
    return new Promise((resolve, reject) => {
      axios.get(lis_user)
        .then(response => {
          commit('GET_LIST_SALER', response);
          return resolve(response);
        })
        .catch(error => {
          return reject(error);
        })
    })
};

export const getListPhotographer = ({commit}) => {
    var lis_user = Url.url+'/api/user/list/select?department_id=3&type=1';
    // var lis_user = Url.url+'/api/user/list/department_id?type=3';
    return new Promise((resolve, reject) => {
      axios.get(lis_user)
        .then(response => {
          commit('GET_LIST_PHOTOGRAPHER', response);
          return resolve(response);
        })
        .catch(error => {
          return reject(error);
        })
    })
};

export const getListPhotoshoper = ({commit}) => {
    var lis_user = Url.url+'/api/user/list/select?department_id=5&type=1';
    // var lis_user = Url.url+'/api/user/list/department_id?type=5';
    return new Promise((resolve, reject) => {
      axios.get(lis_user)
        .then(response => {
          commit('GET_LIST_PHOTOSHOPER', response);
          return resolve(response);
        })
        .catch(error => {
          return reject(error);
        })
    })
};
