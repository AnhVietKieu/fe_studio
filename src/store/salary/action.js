import Url from "@/api/local"

export const getSalaryEmployee = ({commit}) => {
  var lis = Url.url+'/api/listSalary?payroll_type=employee';
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const getSalaryCTV = ({commit}) => {
  var lis = Url.url+'/api/listSalary?payroll_type=ctv';
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};