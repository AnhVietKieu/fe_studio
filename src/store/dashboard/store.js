
import * as actions from "./action";

const store = {
  state: {
    revenue: null,
  },
  getters: {
    revenue: state => state.revenue,
  },
  mutations: {
    GET_REVENUE: (state, payload) => {
      state.listExpense = payload.data.success;
    },
  },
  actions
};
export default store;
