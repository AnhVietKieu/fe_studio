import Url from "@/api/local"

export const getDataForDashboard = ({commit}) => {
  var lis = Url.url+'/api/dashboard';
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};
