import Url from "@/api/local"

export const getPaginativeCategory = ({commit}, type) => {
    var lis = Url.url+'/api/category?type=' + type;
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

var lis = Url.url+'/api/category/list/select';
export const getListCategory = ({commit}) => {
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                commit('GET_LIST_CATEGORY', response);
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getListCategoryCombo = ({commit}) => {
  var Comb = Url.url+'/api/category/list/combo';
  return new Promise((resolve, reject) => {
    axios.get(Comb)
      .then(response => {
        commit('GET_LIST_COMBO', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const getComboByCategory = ({commit}, category_id) => {
  var listC = Url.url+'/api/category/' + category_id + '/listcombo';
  return new Promise((resolve, reject) => {
    axios.get(listC)
      .then(response => {
        commit('LIST_COMBO_BY_CATEGORY', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}

// console.log($store.state.count);
// export const getCategory = ({commit}, opts) => {
//     return new Promise((resolve, reject) => {
//         axios.get('https://apibkl.etstech.vn/api/category' + opts)
//             .then(response => {
//                 return resolve(response);
//             })
//             .catch(error => {
//                 return reject(error);
//             })
//     })
// }
var cre = Url.url+'/api/category/create';
export const createCategory = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios.post(cre, {
            ...opts
        }).then(response => {
            console.log(cre);
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}
export const updateCategory = ({commit}, opts) => {
var edi = Url.url+'/api/category/' + opts.id + '/update';
    return new Promise((resolve, reject) => {
        axios.post(edi, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const deleteCategory = ({commit}, opts) => {
    var del = Url.url+'/api/category/delete';
    return new Promise((resolve, reject) => {
        axios.post(del, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}
