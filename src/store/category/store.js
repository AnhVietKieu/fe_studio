import * as actions from "./action";

const store = {
    state: {
        listCategory: null,
        listCombo: null,
        comboByCategory: null,
    },
    getters: {
        listCategory: state => state.listCategory,
        listCombo: state => state.listCombo,
        comboByCategory: state => state.comboByCategory,
    },
    mutations: {
        GET_LIST_CATEGORY: (state, payload) => {
            state.listCategory = payload.data.success;
        },
        GET_LIST_COMBO: (state, payload) => {
          state.listCombo = payload.data.success;
        },
        LIST_COMBO_BY_CATEGORY: (state, payload) => {
          state.comboByCategory = payload.data.success;
        }
    },
    actions
};
export default store;
