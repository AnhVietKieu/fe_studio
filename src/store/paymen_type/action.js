import Url from "@/api/local"

var lis = Url.url+'/api/payment-type';
export const getListPaymentType = ({commit}) => {
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        commit('GET_LIST_PAYMENT_TYPE', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};
