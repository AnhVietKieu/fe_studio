import * as actions from "./action";

const store = {
  state: {
    listPaymentType: null
  },
  getters: {
    listPaymentType: state => state.listPaymentType
  },
  mutations: {
    GET_LIST_PAYMENT_TYPE: (state, payload) => {
      state.listPaymentType = payload.data.success;
    }
  },
  actions
};
export default store;
