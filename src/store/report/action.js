import Url from "@/api/local"

export const getDataForReport = (context, params) => {
  var lis = Url.url+'/api/report?search=' + JSON.stringify(params);
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};
