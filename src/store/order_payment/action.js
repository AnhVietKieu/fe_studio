import Url from "@/api/local"

export const getListPaymentByOrder = ({commit}, order_id) => {
  var listByOrder = Url.url+'/api/payment-history/' + order_id + '/order';
  return new Promise((resolve, reject) => {
    axios.get(listByOrder)
      .then(response => {
        commit('GET_LIST_ORDER_PAYMENT', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const getListPaymentByOrderHistory = ({commit}, id) => {
  var listByOrderHistory = Url.url+'/api/payment-history/' + id + '/payment';
  return new Promise((resolve, reject) => {
    axios.get(listByOrderHistory)
      .then(response => {
        commit('GET_LIST_ORDER_HISTORY', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};


export const deletePay = ({commit}, opts) => {
  var del = Url.url+'/api/payment-history/delete';
  return new Promise((resolve, reject) => {
    axios.post(del, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}
export const updatePayment = ({commit}, opts) => {
  var edi = Url.url+'/api/payment-history/' + opts.id + '/update';
  return new Promise((resolve, reject) => {
    axios.post(edi, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

var cre = Url.url+'/api/payment-history/create';
export const createPaymentHistory = ({commit}, opts) => {
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
};


