import * as actions from "./action";

const store = {
  state: {
    listOrderPayment: null,
    listOrderHistory: null
  },
  getters: {
    listOrderPayment: state => state.listOrderPayment,
    listOrderHistory: state => state.listOrderHistory
  },
  mutations: {
    GET_LIST_ORDER_PAYMENT: (state, payload) => {
      state.listOrderPayment = payload.data.success;
    },
    GET_LIST_ORDER_HISTORY: (state, payload) => {
      state.listOrderHistory = payload.data.success;
    }
  },
  actions
};
export default store;
