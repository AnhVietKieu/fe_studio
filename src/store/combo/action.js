import Url from "@/api/local"

export const getPaginativeCombo = ({commit}, type) => {
    var lis = Url.url+'/api/category?type=' + type;
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

// export const getListCar = ({commit}) => {
//     return new Promise((resolve, reject) => {
//         axios.get('/api/cars/list/select')
//             .then(response => {
//                 commit(' ', response);
//                 return resolve(response);
//             })
//             .catch(error => {
//                 return reject(error);
//             })
//     })
// };

export const getCombo= ({commit}, opts) => {
  var vie = Url.url+'/api/combo/';
  return new Promise((resolve, reject) => {
    axios.get(vie+ opts)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}

var cre = Url.url+'/api/combo/create';
export const createCombo = ({commit}, opts) => {
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const updateCombo = ({commit}, opts) => {
  var edi = Url.url+'/api/combo/' + opts.id + '/update';
  return new Promise((resolve, reject) => {
    axios.post(edi, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const deleteCombo = ({commit}, opts) => {
  var del = Url.url+'/api/combo/delete';
  return new Promise((resolve, reject) => {
    axios.post(del, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const getComboToOrder= ({commit}, opts) => {
  var ord = Url.url+'/api/combo/'+ opts + '/order';
  return new Promise((resolve, reject) => {
    axios.get(ord)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}
