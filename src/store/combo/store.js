import * as actions from "./action";

const store = {
  state: {
    listCombos: null
  },
  getters: {
    listCombos: state => state.listCombos
  },
  mutations: {
    GET_LIST_COMBOS: (state, payload) => {
      state.listCombos = payload.data.success;
    }
  },
  actions
};
export default store;
