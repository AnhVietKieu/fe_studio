import Url from "@/api/local"

export const getComment = ({commit}, opts) => {
  var vie = Url.url+'/api/order-comment/';
  return new Promise((resolve, reject) => {
    axios.get(vie+ opts)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}

var cre = Url.url+'/api/order-comment/create';
export const createCommnent = ({commit}, opts) => {
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const updateComment = ({commit}, opts) => {
  var edi = Url.url+'/api/order-comment/' + opts.id + '/update';
  return new Promise((resolve, reject) => {
    axios.post(edi, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const deleteComment = ({commit}, opts) => {
  var del = Url.url+'/api/order-comment/delete';
  return new Promise((resolve, reject) => {
    axios.post(del, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}
