import * as actions from "./action";

const store = {
  state: {
    listOrderComment: null
  },
  getters: {
    listOrderComment: state => state.listOrderComment
  },
  mutations: {
    GET_LIST_ORDER_COMMMENT: (state, payload) => {
      state.listOrderComment = payload.data.success;
    }
  },
  actions
};
export default store;
