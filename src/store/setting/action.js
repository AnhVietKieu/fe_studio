import Url from "@/api/local"

export const getConfigSalary = ({commit}, type) => {
    var lis = Url.url+'/api/config_salary?type=' + type;
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};


export const createConfigSalary = ({commit}, opts) => {
  var lis = Url.url+'/api/config_salary/create';
  return new Promise((resolve, reject) => {
    axios.post(lis,{
      ...opts
    })
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};
export const editConfigSalary = ({commit}, opts) => {
  var lis = Url.url+'/api/config_salary/'+opts.id+'/update';
  return new Promise((resolve, reject) => {
    axios.post(lis,{
      ...opts
    })
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const getConfigSalaryOfUser = ({commit}, userId) => {
  var lis = Url.url+'/api/config_salary?filters[EXACT][user_id][0]='+userId;
  return new Promise((resolve, reject) => {
    axios.get(lis)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};
export const createConfigSalaryPhotoshop = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios.post(Url.url+'/api/config_salary/create', {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}
