import Url from "@/api/local"

// export const getPaginativeOrder = ({commit}, page) => {
//   var lis = Url.url+'/api/order?page=' + page;
//   return new Promise((resolve, reject) => {
//     axios.get(lis)
//       .then(response => {
//         return resolve(response);
//       })
//       .catch(error => {
//         return reject(error);
//       })
//   })
// };
export const getListIncurredByOrder = ({commit}, order_id) => {
  var lis_by_order = Url.url+'/api/order_incurred/' + order_id + '/listorder';
  return new Promise((resolve, reject) => {
    axios.get(lis_by_order)
      .then(response => {
        commit('GET_LIST_ORDER_INCURRED', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

var cre = Url.url+'/api/order_incurred/create';
export const createOrderIncurred = ({commit}, opts) => {
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
};

export const updateOrderIncurred = ({commit}, opts) => {
var edi = Url.url+'/api/order_incurred/' + opts.id + '/update';
    return new Promise((resolve, reject) => {
        axios.post(edi, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteOrderIncurred = ({commit}, opts) => {
    var del = Url.url+'/api/order_incurred/delete';
    return new Promise((resolve, reject) => {
        axios.post(del, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

// export const getOrderIncurred = ({commit}, opts) => {
// var vie = Url.url+'/api/order_incurred/'  + opts.id + '/listorder';
//     return new Promise((resolve, reject) => {
//         axios.get(vie+ opts)
//             .then(response => {
//                 return resolve(response);
//             })
//             .catch(error => {
//                 return reject(error);
//             })
//     })
// }

// var cre = Url.url+'/api/order-comment/create';
// export const createCommnent = ({commit}, opts) => {
//   return new Promise((resolve, reject) => {
//     axios.post(cre, {
//       ...opts
//     }).then(response => {
//       return resolve(response);
//     }).catch(error => {
//       return reject(error);
//     })
//   })
// }
// export const updateComment = ({commit}, opts) => {
//   var edi = Url.url+'/api/order-comment/' + opts.id + '/update';
//   return new Promise((resolve, reject) => {
//     axios.post(edi, {
//       ...opts
//     }).then(response => {
//       return resolve(response);
//     }).catch(error => {
//       return reject(error);
//     })
//   })
// }

// export const deleteComment = ({commit}, opts) => {
//   var del = Url.url+'/api/order-comment/delete';
//   return new Promise((resolve, reject) => {
//     axios.post(del, {
//       ...opts
//     }).then(response => {
//       return resolve(response);
//     }).catch(error => {
//       return reject(error);
//     })
//   })
// }
