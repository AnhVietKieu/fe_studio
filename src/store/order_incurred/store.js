import * as actions from "./action";

const store = {
  state: {
    listOrderIncurred: null
  },
  getters: {
    listOrderIncurred: state => state.listOrderIncurred
  },
  mutations: {
    GET_LIST_ORDER_INCURRED: (state, payload) => {
      state.listOrderIncurred = payload.data.success;
    }
  },
  actions
};
export default store;
