import * as actions from "./action";

const store = {
    state: {
        listIncurred: null
    },
    getters: {
        listIncurred: state => state.listIncurred
    },
    mutations: {
        GET_LIST_INCURRED: (state, payload) => {
            state.listIncurred = payload.data.success;
        }
    },
    actions
};
export default store;
