import Url from "@/api/local"

export const getPaginativeIncurred = ({commit}, page) => {
    var lis = Url.url+'/api/incurred?page=' + page;
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

var lis = Url.url+'/api/incurred/list/select';
export const getListIncurred = ({commit}) => {
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                commit('GET_LIST_INCURRED', response);
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};
// export const getListCar = ({commit}) => {
//     return new Promise((resolve, reject) => {
//         axios.get('/api/cars/list/select')
//             .then(response => {
//                 commit(' ', response);
//                 return resolve(response);
//             })
//             .catch(error => {
//                 return reject(error);
//             })
//     })
// };
// console.log($store.state.count);
// export const getCategory = ({commit}, opts) => {
//     return new Promise((resolve, reject) => {
//         axios.get('https://apibkl.etstech.vn/api/category' + opts)
//             .then(response => {
//                 return resolve(response);
//             })
//             .catch(error => {
//                 return reject(error);
//             })
//     })
// }
var cre = Url.url+'/api/incurred/create';
export const createIncurred = ({commit}, opts) => {
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}
export const updateIncurred = ({commit}, opts) => {
  var edi = Url.url+'/api/incurred/' + opts.id + '/update';
  return new Promise((resolve, reject) => {
    axios.post(edi, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const deleteIncurred = ({commit}, opts) => {
  var del = Url.url+'/api/incurred/delete';
  return new Promise((resolve, reject) => {
    axios.post(del, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}
