import Url from "@/api/local"
export const getPaginativeConfig = ({commit}, opts ) => {
    if(opts.page || opts.customer_id || opts.search )
    {
      var lis = Url.url+'/api/customer?page=' + opts.page + '&customer_id=' + opts.customer_id + '&search=' + opts.search;
    }
    else {
      var lis = Url.url+'/api/customer?page=' + opts.page  + '&search=' + opts.search;
    }
    return new Promise((resolve, reject) => {
          axios.get(lis)
              .then(response => {
                  return resolve(response);
              })
              .catch(error => {
                  return reject(error);
              })
      })
  };
  
  export const getListSale = ({commit}) => {
      var lis = Url.url+'/api/config_salary';
      return new Promise((resolve, reject) => {
          axios.get(lis)
              .then(response => {
                  commit('GET_LIST_CUSTOMER', response);
                  return resolve(response);
              })
              .catch(error => {
                  return reject(error);
              })
      })
  };
  
  export const getCustomer = ({commit}, opts) => {
      var vie = Url.url+'/api/config_salary/';
      return new Promise((resolve, reject) => {
          axios.get(vie+ opts)
              .then(response => {
                  return resolve(response);
              })
              .catch(error => {
                  return reject(error);
              })
      })
  }
  var cre = Url.url+'/api/config_salary/create';
  export const createSale = ({commit}, opts) => {
      return new Promise((resolve, reject) => {
          axios.post(cre, {
              ...opts
          }).then(response => {
              return resolve(response);
          }).catch(error => {
              return reject(error);
          })
      })
  }
  export const updateSale = ({commit}, opts) => {
  var edi = Url.url+'/api/config_salary/' + opts.id + '/update';
      return new Promise((resolve, reject) => {
          axios.post(edi, {
              ...opts
          }).then(response => {
              return resolve(response);
          }).catch(error => {
              return reject(error);
          })
      })
  }