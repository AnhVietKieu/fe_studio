import * as actions from "./action";

const store = {
  state: {
    report: null,
  },
  getters: {
    report: state => state.report,
  },
  mutations: {
    GET_REPORT: (state, payload) => {
      state.report = payload.data.success;
    },
  },
  actions
};
export default store;
