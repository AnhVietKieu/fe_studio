import * as actions from "./action";

const store = {
    state: {
        listService: null
    },
    getters: {
        listService: state => state.listService
    },
    mutations: {
        GET_LIST_SERVICE: (state, payload) => {
            state.listService = payload.data.success;
        }
    },
    actions
};
export default store;
