import Url from "@/api/local"

export const getPaginativeService = ({commit}, page) => {
    var lis = Url.url+'/api/service?page=' + page;
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getListService = ({commit}) => {
    var lis = Url.url+'/api/service/list/select';
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                commit('GET_LIST_SERVICE', response);
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getService = ({commit}, opts) => {
var vie = Url.url+'/api/service/';
    return new Promise((resolve, reject) => {
        axios.get(vie+ opts)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
}
var cre = Url.url+'/api/service/create';
export const createService = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios.post(cre, {
            ...opts
        }).then(response => {
            console.log(cre);
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}
export const updateService = ({commit}, opts) => {
var edi = Url.url+'/api/service/' + opts.id + '/update';
    return new Promise((resolve, reject) => {
        axios.post(edi, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const deleteService = ({commit}, opts) => {
    var del = Url.url+'/api/service/delete';
    console.log(del);
    return new Promise((resolve, reject) => {
        axios.post(del, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}
