import Url from "@/api/local"

export const getPaginativeOrder = ({commit}, opts) => {
  if(opts.status || opts.status == 0 ) {
    var lis = Url.url+'/api/order?page=' + opts.page + '&status=' + opts.status;
  }else {
    var lis = Url.url+'/api/order?page=' + opts.page;
  }
	return new Promise((resolve, reject) => {
	axios.get(lis)
	  .then(response => {
	    return resolve(response);
	  })
	  .catch(error => {
	    return reject(error);
	  })
	})
};

export const getOrder = ({commit}, opts) => {
  var vie = Url.url+'/api/order/';
  return new Promise((resolve, reject) => {
    axios.get(vie+ opts)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}

var cre = Url.url+'/api/order/create';
export const createOrderNew = ({commit}, opts) => {
  return new Promise((resolve, reject) => {
    axios.post(cre, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
};
// lay ra danh sach cac order có sẵn
export const getListOrder = ({commit}, status) => {
  var lis_order = Url.url+'/api/order/list/select?status=' + status;
  return new Promise((resolve, reject) => {
    axios.get(lis_order)
      .then(response => {
        commit('GET_LIST_ORDER', response);
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

export const deleteOrders = ({commit}, opts) => {
  var del = Url.url+'/api/order/delete';
  return new Promise((resolve, reject) => {
    axios.post(del, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

// timline dự kiến của order
export const getExpectedTimeline = ({commit}, order_id) => {
  var expected = Url.url+'/api/order/' + order_id + '/expected-timeline';
  return new Promise((resolve, reject) => {
    axios.get(expected)
      .then(response => {
          return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
};

//danh sách mail đã gửi
export const getListEmail = ({commit}, opts) => {
    var vie = Url.url+'/api/email/lists?order_id=' + opts.order_id + '&customer_id=' + opts.customer_id;
    return new Promise((resolve, reject) => {
        axios.get(vie)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
}

// update thời gian chụp ảnh
export const addDatePhoto = ({commit}, opts) => {
  var photographed = Url.url+'/api/order/add_date_photo';
  return new Promise((resolve, reject) => {
    axios.post(photographed, {
        ...opts
    })
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}

export const getOrderToEdit = ({commit}, order_id) => {
  var orderToEdit = Url.url+'/api/order/' + order_id + '/list-edit';
  return new Promise((resolve, reject) => {
    axios.get(orderToEdit)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}

export const updateOrder = ({commit}, opts) => {
  var update = Url.url+'/api/order/' + opts.id + '/update';
  return new Promise((resolve, reject) => {
    axios.post(update, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
};

export const getListNumberOrder = ({commit}, order_id) => {
  var listNumber = Url.url+'/api/order/list/number';
  return new Promise((resolve, reject) => {
    axios.get(listNumber)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        return reject(error);
      })
  })
}

//notification
export const pushNotification = ({commit}, opts) => {
  console.log(opts);
  var noti = 'https://notify.etstech.vn/api/notification';
  return new Promise((resolve, reject) => {
    axios.post(noti, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
};

