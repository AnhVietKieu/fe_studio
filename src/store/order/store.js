import * as actions from "./action";

const store = {
  state: {
    listOrder: null
  },
  getters: {
    listOrder: state => state.listOrder
  },
  mutations: {
    GET_LIST_ORDER: (state, payload) => {
      state.listOrder = payload.data.success;
    }
  },
  actions
};
export default store;
