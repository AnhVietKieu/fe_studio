import * as actions from "./action";

const store = {
    state: {
        listCustomer: null
    },
    getters: {
        listCustomer: state => state.listCustomer
    },
    mutations: {
      GET_LIST_CUSTOMER: (state, payload) => {
            state.listCustomer = payload.data.success;
        }
    },
    actions
};
export default store;
