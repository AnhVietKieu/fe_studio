import Url from "@/api/local"

export const getPaginativeCustomer = ({commit}, opts ) => {
  if(opts.page || opts.customer_id || opts.search )
  {
    var lis = Url.url+'/api/customer?page=' + opts.page + '&customer_id=' + opts.customer_id + '&search=' + opts.search;
  }
  else {
    var lis = Url.url+'/api/customer?page=' + opts.page  + '&search=' + opts.search;
  }
  return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getListCustomer = ({commit}) => {
    var lis = Url.url+'/api/customer/list/select';
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                commit('GET_LIST_CUSTOMER', response);
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getCustomer = ({commit}, opts) => {
    var vie = Url.url+'/api/customer/';
    return new Promise((resolve, reject) => {
        axios.get(vie+ opts)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
}
var cre = Url.url+'/api/customer/create';
export const createCustomer = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios.post(cre, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}
export const updateCustomer = ({commit}, opts) => {
var edi = Url.url+'/api/customer/' + opts.id + '/update';
    return new Promise((resolve, reject) => {
        axios.post(edi, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const deleteCustomer = ({commit}, opts) => {
    var del = Url.url+'/api/customer/delete';
    return new Promise((resolve, reject) => {
        axios.post(del, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const historyOrders = ({commit}, opts) => {
  var his = Url.url+'/api/customer/' + opts + '/order';
  return new Promise((resolve, reject) => {
    axios.get(his, {
      ...opts
    }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    })
  })
}

export const listsAdvancedSearch = ({commit}, opts) => {
    var advanced = Url.url+'/api/customer/list/search?page=' + opts.page;
    return new Promise((resolve, reject) => {
        axios.post(advanced, {
            ...opts
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}