import * as actions from "./action";

const store = {
  state: {
    listOrderJob: null
  },
  getters: {
    listOrderJob: state => state.listOrderJob
  },
  mutations: {
    GET_LIST_ORDER_JOB: (state, payload) => {
      state.listOrderJob = payload.data.success;
    }
  },
  actions
};
export default store;
