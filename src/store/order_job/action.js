import Url from "@/api/local"

export const getListOrderJob = ({commit}, filter) => {
    var lis = Url.url + '/api/order-job';
    return new Promise((resolve, reject) => {
        axios.post(lis, {
            ...filter
        })
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getListJob = ({commit}, opts) => {
    var lis = Url.url + '/api/order-job/listJob?page=' + opts.page;
    return new Promise((resolve, reject) => {
        axios.get(lis)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
};

export const getListJobByOrder = ({commit}, order_id) => {
    var listByOrder = Url.url + '/api/order-job/' + order_id + '/list-job';
    return new Promise((resolve, reject) => {
        axios.get(listByOrder)
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            })
    })
}

export const viewJob = ({commit}, job_id) => {
    var view_job = Url.url + '/api/order-job/' + job_id + '/view';
    return new Promise((resolve, reject) => {
        axios.get(view_job, {
            ...job_id
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const createJob = ({commit}, order_id) => {
    var cre = Url.url + '/api/order-job/' + order_id + '/create';
    return new Promise((resolve, reject) => {
        axios.get(cre, {
            ...order_id
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}

export const listNumber = ({commit}, order_id) => {
    var cre = Url.url + '/api/order-job/listnumber';
    return new Promise((resolve, reject) => {
        axios.get(cre, {
            ...order_id
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
}