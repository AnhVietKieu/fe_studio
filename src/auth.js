// import bearer from "@websanova/vue-auth/drivers/auth/bearer"
import axios from "@websanova/vue-auth/drivers/http/axios.1.x"
import router from "@websanova/vue-auth/drivers/router/vue-router.2.x"

const config = {
    auth:{
        request: function (req, token) {
            this.options.http._setHeaders.call(this, req, {Authorization: 'Bearer ' + token});
        },
        response:(res) =>{
            let token = (res.data&&res.data.success&&res.data.success.access_token)?res.data.success.access_token:null;
            if(token) return token.trim();
        }
    },
    http:axios,
    router:router,
    authRedirect: {path: '/auth/login'},
    tokenStore: ['cookie'],
    tokenDefaultName:"access_token",
    rolesVar:"scopes",
    notFoundRedirect:{
        path:"/dashboard"
    },

    registerData: {
        url: "/api/auth/register",
        method: "POST",
        redirect: "/users",
    },

    loginData: {
        url: "/api/auth/login",
        method: "POST",
        fetchUser: true,
    },

    logoutData: {
        url: "/api/auth/logout",
        method: "POST",
        redirect: "/auth/login",
        makeRequest: true
    },

    fetchData: {
        url: "/api/auth/user",
        method: "GET",
        enabled: true
    },

    refreshData: {
        url: '/api/auth/refresh',
        method: 'GET',
        enabled: false,
        interval: 30
    },

    parseUserData (data) {
        return data || {}
    },
}

export default config