import Vue from 'vue';
import alert from 'sweetalert2';

Vue.prototype.$alert = alert;

Vue.prototype.$confirm = (title, msg, okLabel, cancelLabel) => {
    return new Promise((resolve, reject) => {
        alert.fire({
            title: title || 'Confirm',
            text: msg,
            // type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#F44336',
            confirmButtonText: okLabel || 'Ok',
            cancelButtonText: cancelLabel || 'Cancel',
            width: 300,
            customClass: 'swal-height',
        }).then((res) => {
            if (res.value) {
                return resolve();
            }
        });
    });
}

Vue.prototype.$logout = (title, msg, okLabel, cancelLabel) => {
    return new Promise((resolve, reject) => {
        alert({
            title: title || 'Confirm',
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff3860',
            confirmButtonText: okLabel || 'Ok',
            cancelButtonText: cancelLabel || 'Cancel',
        }).then((res) => {
            if (res.value) {
                return resolve();
            }
        });
    });
}
// Vue.prototype.$checkBoard = (title, msg, okLabel, cancelLabel) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             title: title || 'Confirm',
//             text: msg,
//             type: "warning",
//             // showCancelButton: true,
//             confirmButtonColor: '#ffdd57',
//             confirmButtonText: okLabel || 'Quay lại'
//             // cancelButtonText: cancelLabel || 'Quay lại',
//         }).then((res) => {
//             if (res.value) {
//                 return resolve();
//             }
//         });
//     });
// }

// Vue.prototype.$message = (msg, type) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             type: type || 'success',
//             title: msg || '',
//             showConfirmButton: false,
//             timer: 1000
//         });
//     });
// }


Vue.prototype.$success = (msg, type) => {
    return new Promise((resolve, reject) => {
        alert.fire({
            type: 'success',
            title: msg || 'Thành công',
            showConfirmButton: false,
            customClass: 'swal-height',
            timer: 1200
        });
    });
}

Vue.prototype.$email = (msg, type) => {
    return new Promise((resolve, reject) => {
        alert.fire({
            type: 'success',
            title: msg || 'Mật khẩu đã được Reset',
            showConfirmButton: false,
            customClass: 'swal-height',
            timer: 1200
        });
    });
}

// Vue.prototype.$logout = (title, msg, okLabel, cancelLabel) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             title: title || 'Confirm',
//             text: msg,
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: '#ff3860',
//             confirmButtonText: okLabel || 'Ok',
//             cancelButtonText: cancelLabel || 'Cancel',
//         }).then((res) => {
//             if (res.value) {
//                 return resolve();
//             }
//         });
//     });
// }


// Vue.prototype.$loginsc = (msg, type) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             type: 'success',
//             title: msg || 'Đăng nhập thành công',
//             showConfirmButton: false,
//             timer: 4000
//         });
//     });
// }

Vue.prototype.$error = (text,msg, type) => {
    let mes = text;
    return new Promise((resolve, reject) => {
        alert.fire({
            type: 'error',
            title: 'Lỗi: ' + mes,
            // text: 'Bạn đã nhập thiếu các trường cần thiết!',
            showConfirmButton: false,
            width: 400,
            timer: 2000
        });
    });
}

// Vue.prototype.$salaries = (text,msg, type) => {
//     let mes = text;
//     return new Promise((resolve, reject) => {
//         alert({
//             type: 'info',
//             title: 'Lỗi: ' + mes,
//             text: 'Hệ thống sẽ quay trở lại danh sách bảng lương!',
//             showConfirmButton: false,
//             width: 400,
//             timer: 3000
//         });
//     });
// }

// Vue.prototype.$checkMonth = (msg, type) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             type: 'info',
//             title: 'Thời gian chọn đã có bảng lương!',
//             text: 'Vui lòng chọn thời gian khác!',
//             showConfirmButton: false,
//             width: 400,
//             timer: 3000
//         });
//     });
// }

// Vue.prototype.$errorList = (msg, type) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             type: 'error',
//             title: 'Lỗi dữ liệu',
//             text: 'Dữ liệu bị lỗi, Hệ thống tự động quay lại màn hình trước đó!',
//             showConfirmButton: false,
//             width: 400,
//             timer: 2000
//         });
//     });
// }

// Vue.prototype.$checkStatus = (msg, type) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             type: 'error',
//             title: 'Tài khoản đã khóa',
//             // text: 'Hệ thống tự động quay lại màn hình trước đó!',
//             showConfirmButton: false,
//             width: 400,
//             timer: 2000
//         });
//     });
// }

Vue.prototype.$warning = (msg, type) => {
    return new Promise((resolve, reject) => {
        alert.fire({
            type: 'error',
            title: msg || 'Bạn đã nhập thiếu các trường cần thiết!',
            showConfirmButton: false,
            width: 400,
            timer: 1500
        });
    });
}
Vue.prototype.$search = (msg, type) => {
    return new Promise((resolve, reject) => {
        alert.fire({
            type: 'info',
            title: msg || 'Không tìm thấy bản ghi nào!',
            showConfirmButton: true,
            width: 400,
            timer: 1500
        });
    });
}
// Vue.prototype.$info = (msg, type) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             type: 'info',
//             title: msg || '',
//             showConfirmButton: false,
//             timer: 1000
//         });
//     });
// }

// Vue.prototype.$statistics = (msg, type) => {
//     return new Promise((resolve, reject) => {
//         alert({
//             type: 'info',
//             title: msg || 'Khách hàng chưa có hóa đơn!',
//             text: 'Bạn đã tạo hóa đơn cho khách hàng này!',
//             showConfirmButton: true,
//             timer: 4000,
//             confirmButtonColor: '#ff3860',
//             confirmButtonText: 'Xác nhận',
//         });
//     });
// }