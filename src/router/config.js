import {AuthLayout, DefaultLayout} from "@/components/layouts"
import Dashboard from '@/views/Dashboard'
import Forbidden from '@/views/error/Deny.vue'

import Customers from '@/views/customers/Customers'
import CustomersCreate from '@/views/customers/Create'
import CustomersEdit from '@/views/customers/Edit'
import CustomersView from '@/views/customers/View'

// import Users from '@/views/users/Users'
// import UsersCreate from '@/views/users/Create'
import UsersList from '@/views/users/Listusers'

import Suppliers from '@/views/suppliers/Suppliers'
import SuppliersView from '@/views/suppliers/View'
import SuppliersCreate from '@/views/suppliers/Create'
import SuppliersEdit from '@/views/suppliers/Edit'

import Services from '@/views/service/Service'
import ServicesView from '@/views/service/View'
import ServicesCreate from '@/views/service/Create'
import ServicesEdit from '@/views/service/Edit'

import Combos from '@/views/combo/Combo'
import CombosCreate from '@/views/combo/Create'
import CombosEdit from '@/views/combo/Edit'
import CombosView from '@/views/combo/View'

import Category from '@/views/category/Category.vue'
import CategoryCreate from '@/views/category/Create'

import CategoryCombo from '@/views/categorycombo/Create'

import Incurred from '@/views/incurred/Incurred'
import IncurredCreate from '@/views/incurred/Create'

import Profile from '@/views/profile/profile'
// import ProfileEdit from '@/views/profile/ProfileEdit'

import Order from '@/views/orders/Orders'
import OrderCreate from '@/views/orders/Create'
import OrderEdit from '@/views/orders/Edit'
import OrderView from '@/views/orders/View'

import Assigned from '@/views/order_job/OrderJob'
import OrderJob from '@/views/order_job/Listorderjob'
import OrderJobCreate from '@/views/order_job/Create'
import OrderJobView from '@/views/order_job/View'
// import OrderJobCalendar from '@/views/order_job/Calendar'

import Expense from '@/views/expense/Listexpense'
import ExpenseCreate from '@/views/expense/Create'


import Salary from '@/views/salary/Salary'

import CreateSalary from '@/views/salary/CreateSalary'

// config salary

import ConfigSale from '@/views/config_sale/ListSalary'
import ConfigPartner from '@/views/config_partner/ListPartner'

import CreateSalarySale from '@/views/config_sale/CreateSalary'
///

import Report from '@/views/Report'

// setting
import Setting from '@/views/setting/Setting'
import Photographer from '@/views/setting/Photographer'
import Photoshoper from '@/views/setting/Photoshoper'
import Saler from '@/views/setting/Saler'

export const publicRoute = [
  {path: "*", component: () => import(/* webpackChunkName: "errors-404" */ "@/views/error/NotFound.vue")},
  {
    path: "/auth",
    component: AuthLayout,
    meta: {title: "Login", auth: false},
    redirect: "/auth/login",
    hidden: true,
    children: [
      {
        path: "login",
        name: "login",
        meta: {title: "Login", auth: false},
        component: () => import(/* webpackChunkName: "login" */ "@/views/auth/Login.vue")
      }
    ]
  },

  {
    path: "/404",
    name: "404",
    meta: {title: "Not Found", auth: false},
    component: () => import(/* webpackChunkName: "errors-404" */ "@/views/error/NotFound.vue")
  },

  {
    path: "/500",
    name: "500",
    meta: {title: "Server Error", auth: false},
    component: () => import(/* webpackChunkName: "errors-500" */ "@/views/error/Error.vue")
  }
]

export const protectedRoute = [
  {
    path: "/",
    component: DefaultLayout,
    meta: {title: "Trang chủ", auth: true},
    redirect: "/dashboard",
    children: [
      {
        path: "/dashboard",
        name: "Dashboard",
        meta: {title: "Danh sách", auth: true},
        component: Dashboard
      },
      {
        path: "/403",
        name: "Forbidden",
        meta: {title: "Access Denied", hiddenInMenu: true, auth: true},
        component: Forbidden
      }
    ]
  },

  {
    path: "/report",
    component: DefaultLayout,
    meta: {title: "Tài khoản", auth: true},
    redirect: "/report",
    children: [
      {
        path: "/report",
        name: "report",
        meta: {title: "Báo cáo", auth: true},
        component: Report,
      },
    ]
  },

  {
    path: "/users",
    component: DefaultLayout,
    meta: {title: "Tài khoản", auth: true},
    redirect: "/users",
    children: [
      {
        path: "/users",
        name: "users",
        meta: {title: "Danh sách", auth: {roles: 'LIST_USER', forbiddenRedirect: '/403'}},
        component: UsersList,
      },
    ]
  },

  {
    path: "/customers",
    component: DefaultLayout,
    meta: {title: "Khách hàng", auth: true},
    redirect: "/customers",
    children: [
      {
        path: "/customers",
        name: "customers",
        meta: {title: "Danh sách", auth: {roles: 'LIST_CUSTOMER', forbiddenRedirect: '/403'}},
        component: Customers,
      },
      {
        path: "/customers/create",
        name: "customers-create",
        meta: {title: "Thêm mới", auth: {roles: 'CREATE_CUSTOMER', forbiddenRedirect: '/403'}},
        component: CustomersCreate
      },
      {
        path: "/customers/edit/:id",
        name: "customers-edit",
        meta: {title: "Chỉnh sửa", auth: {roles: 'EDIT_CUSTOMER', forbiddenRedirect: '/403'}},
        component: CustomersEdit
      },
      {
        path: "/customers/view/:id",
        name: "customers-view",
        meta: {title: "Chi tiết", auth: {roles: 'LIST_CUSTOMER', forbiddenRedirect: '/403'}},
        component: CustomersView
      },
    ]
  },

  {
    path: "/combo",
    component: DefaultLayout,
    meta: {title: "Gói chụp", group: "combos", auth: true},
    redirect: "/combos",
    children: [
      {
        path: "/combo",
        name: "Combos",
        meta: {title: "Danh sách", auth: {roles: 'LIST_COMBO', forbiddenRedirect: '/403'}},
        component: Combos,
      },
      {
        path: "/combo/create",
        name: "combo-create",
        meta: {title: "Thêm mới", auth: {roles: 'CREATE_COMBO', forbiddenRedirect: '/403'}},
        component: CombosCreate
      },
      {
        path: "/combo/edit/:id",
        name: "combo-edit",
        meta: {title: "Chỉnh sửa", auth: {roles: 'EDIT_COMBO', forbiddenRedirect: '/403'}},
        component: CombosEdit
      },
      {
        path: "/combo/view/:id",
        name: "combo-view",
        meta: {title: "Chi tiết", auth: {roles: 'LIST_COMBO', forbiddenRedirect: '/403'}},
        component: CombosView
      },
      {
        path: "/combo/categorycombo",
        name: "CategoryCombo",
        meta: {title: "", auth: {roles: 'LIST_CATEGORY', forbiddenRedirect: '/403'}},
        component: CategoryCombo
      }
    ]
  },

  {
    path: "/incurred",
    component: DefaultLayout,
    meta: {title: "Chi phát sinh", auth: true},
    redirect: "/incurred",
    children: [
      {
        path: "/incurred",
        name: "incurred",
        meta: {title: "Danh sách", auth: {roles: 'LIST_INCURRED', forbiddenRedirect: '/403'}},
        component: Incurred,
      },
      {
        path: "/incurred/create",
        name: "incurred-create",
        meta: {title: "Thêm mới", auth: {roles: 'CREATE_INCURRED', forbiddenRedirect: '/403'}},
        component: IncurredCreate
      },
    ]
  },

  {
    path: "/suppliers",
    component: DefaultLayout,
    meta: {title: "Nhà cung cấp", auth: true},
    redirect: "/suppliers",
    children: [
      {
        path: "/suppliers",
        name: "suppliers",
        meta: {title: "Danh sách", auth: {roles: 'LIST_SUPPLIER', forbiddenRedirect: '/403'}},
        component: Suppliers
      },
      {
        path: "/suppliers/view/:id",
        name: "suppliers-view",
        meta: {title: "Chi tiết", auth: {roles: 'CREATE_SUPPLIER', forbiddenRedirect: '/403'}},
        component: SuppliersView
      },
      {
        path: "/suppliers/create",
        name: "suppliers-create",
        meta: {title: "Thêm mới", auth: {roles: 'EDIT_SUPPLIER', forbiddenRedirect: '/403'}},
        component: SuppliersCreate
      },
      {
        path: "/suppliers/edit/:id",
        name: "suppliers-edit",
        meta: {title: "Chỉnh sửa", auth: {roles: 'LIST_SUPPLIER', forbiddenRedirect: '/403'}},
        component: SuppliersEdit
      },
    ]
  },

  {
    path: "/categories",
    component: DefaultLayout,
    meta: {title: "Loại dịch vụ", auth: true},
    redirect: "/categories",
    children: [
      {
        path: "/category",
        name: "category",
        meta: {title: "Danh sách", auth: {roles: 'LIST_CATEGORY', forbiddenRedirect: '/403'}},
        component: Category
      },
      {
        path: "/category/create",
        name: "category-create",
        meta: {title: "Thêm mới", auth: {roles: 'CREATE_CATEGORY', forbiddenRedirect: '/403'}},
        component: CategoryCreate
      },
    ]
  },

  {
    path: "/services",
    component: DefaultLayout,
    meta: {title: "Gói lẻ", icon: "widgets", group: "services", auth: true},
    redirect: "/services",
    children: [
      {
        path: "/services",
        name: "Services",
        meta: {title: "Danh sách", auth: {roles: 'LIST_SERVICE', forbiddenRedirect: '/403'}},
        component: Services
      },
      {
        path: "/services/create",
        name: "services-create",
        meta: {title: "Thêm mới", auth: {roles: 'CREATE_SERVICE', forbiddenRedirect: '/403'}},
        component: ServicesCreate
      },
      {
        path: "/services/view/:id",
        name: "services-view",
        meta: {title: "Chi tiết", auth: {roles: 'LIST_SERVICE', forbiddenRedirect: '/403'}},
        component: ServicesView
      },
      {
        path: "/services/edit/:id",
        name: "services-edit",
        meta: {title: "Chỉnh sửa", auth: {roles: 'EDIT_SERVICE', forbiddenRedirect: '/403'}},
        component: ServicesEdit
      },
      {
        path: "/services/category",
        name: "Category",
        meta: {title: " Danh sách loại dịch vụ", auth: {roles: 'LIST_CATEGORY', forbiddenRedirect: '/403'}},
        component: Category
      },
    ]
  },

  {
    path: "/order",
    component: DefaultLayout,
    meta: {title: "Đặt hàng", icon: "widgets", group: "order", auth: true},
    redirect: "/order",
    children: [
      {
        path: "/order",
        name: "Order",
        meta: {title: "Danh sách", auth: true},
        component: Order
      },
      {
        path: "/order/create",
        name: "order-create",
        meta: {title: "Thêm mới", auth: true},
        component: OrderCreate
      },
      {
        path: "/order/view/:id",
        name: "order-view",
        meta: {title: "Chi tiết"},
        component: OrderView
      },
      {
        path: "/order/edit/:id",
        name: "order-edit",
        meta: {title: "Chỉnh sửa"},
        component: OrderEdit
      },
    ]
  },

  {
    path: "/order-job",
    component: DefaultLayout,
    meta: {title: "Công việc", group: "order-job", auth: true},
    redirect: "/order-job",
    children: [
      {
        path: "/order-job",
        name: "order-job",
        meta: {title: "Phân công công việc"},
        component: OrderJob
      },
      {
        path: "/order-job/assigned/list",
        name: "Assigned",
        meta: {title: "Danh sách", auth: true},
        component: Assigned
      },
      {
        path: "/order-job/create",
        name: "order-job-create",
        meta: {title: "Thêm mới"},
        component: OrderJobCreate
      },
      {
        path: "/order-job/view/:id",
        name: "order-job-view",
        meta: {title: "Chi tiết"},
        component: OrderJobView
      },

    ]
  },
  {
    path: "/expense",
    component: DefaultLayout,
    meta: {title: "Chi tiêu", auth: true},
    redirect: "/expense",
    children: [
      {
        path: "/expense",
        name: "expense",
        meta: {title: "Danh sách", auth: true},
        component: Expense,
      },
      {
        path: "/expense/create",
        name: "ExpenseCreate",
        meta: {title: "Thêm mới", auth: true},
        component: ExpenseCreate
      },
    ]
  },
  {
    path: "/setting",
    component: DefaultLayout,
    meta: {title: "setting", auth: true},
    redirect: "/setting",
    children: [
      {
        path: "/setting",
        name: "Setting",
        redirect: "/setting/photographer",
        component: Setting,
        // meta: {title: "setting", auth: true},
        children: [
          {
            path: "/setting/photographer",
            name: "Photographer",
            meta: {title: "Photographer", auth: true},
            component: Photographer
          },
          {
            path: "/setting/photoshoper",
            name: "Photoshoper",
            meta: {title: "Photoshoper", auth: true},
            component: Photoshoper
          },
          {
            path: "/setting/saler",
            name: "Saler",
            meta: {title: "Saler", auth: true},
            component: Saler
          }
        ]
      },

      // {
      //     path: "/setting/photographer",
      //     name: "Photographer",
      //     meta: {title: "Photographer", auth: true},
      //     component: Photographer
      // },
      // {
      //     path: "/setting/photoshoper",
      //     name: "Photoshoper",
      //     meta: {title: "Photoshoper", auth: true},
      //     component: Photoshoper
      // },
      // {
      //     path: "/setting/saler",
      //     name: "Saler",
      //     meta: {title: "Saler", auth: true},
      //     component: Saler
      // }
    ]
  },


  // {
  //   path: "/order-job",
  //   component: DefaultLayout,
  //   meta: {title: "Công việc", group: "order-job", auth: true},
  //   redirect: "/order-job",
  // children: [
  // {
  //   path: "/order-job",
  //   name: "order-job",
  //   meta: {title: "Danh sách"},
  //   component: OrderJob
  // },
  // {
  //   path: "/order-job/create",
  //   name: "order-job-create",
  //   meta: {title: "Thêm mới"},
  //   component: OrderJobCreate
  // },
  // {
  //   path: "/order-job/view/:id",
  //   name: "order-job-view",
  //   meta: {title: "Chi tiết"},
  //   component: OrderJobView
  // },
  //   ]
  // },
  // {
  //     path: "/assigned",
  //     component: DefaultLayout,
  //     meta: {title: "Đặt Lịch", icon: "widgets", group: "assigned", auth: true},
  //     redirect: "/assigned/list",
  //     children: [
  //         {
  //             path: "/assigned/list",
  //             name: "Assigned",
  //             meta: {title: "Danh sách", auth: true},
  //             component: Assigned
  //         },

  //     ]
  // },
  {
    path: "/salary",
    component: DefaultLayout,
    meta: {title: "Bảng lương", auth: true},
    redirect: "/salary",
    children: [
      {
        path: "/salary",
        name: "salary",
        meta: {title: " Danh sách Bảng lương"},
        component: Salary,
      },
      {
        path: "/salary/create",
        name: "salarycreate",
        meta: {title: " Tạo mới Bảng lương"},
        component: CreateSalary,
      },
    ]
  },

  {
    path: "/profile",
    component: DefaultLayout,
    meta: {title: "Profile", group: "Profile", auth: true},
    redirect: "/profile",
    children: [
      {
        path: "/profile",
        name: "profile",
        meta: {title: "Tài khoản", auth: true},
        component: Profile,
      },
    ]
  },


  {
    path: "/config-salary",
    component: DefaultLayout,
    meta: {title: "Cài đặt lương", group: "config-salary", auth: true},
    redirect: "/config-salary/sale",
    children: [
      {
        path: "/config-salary",
        name: "config_salary",
        meta: {title: "Nhân viên"},
        component: ConfigSale
      },
      {
        path: "/config-salary/sale/create",
        name: "config-salary-sale",
        meta: {title: "Thêm mới"},
        component: CreateSalarySale
      },
      {
        path: "/config-salary/partner",
        name: "config_partner",
        meta: {title: "Cộng tác viên", auth: true},
        component: ConfigPartner
      },

    ]
  }
]
