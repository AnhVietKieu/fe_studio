const Menu = [
  { header: "" },
  {
    title: "Trang Chủ",
    group: "apps",
    icon: "dashboard",
    name: "Dashboard"
  },
  {
    title: "Báo cáo",
    group: "apps",
    icon: "report",
    name: "report"
  },
  {
    title: "Tài Khoản",
    icon: "person",
    name: "users",
    scope: "LIST_USER"
    // component: "Customers",
  },
  {
    title: "Khách Hàng",
    icon: "assignment_ind",
    name: "customers",
    scope: "LIST_CUSTOMER"
    // component: "Customers",
  },
  {
    title: "Nhà Cung Cấp",
    icon: "person_pin",
    name: "suppliers",
    scope: "LIST_SUPPLIER"
  },
  {
    title: "Gói lẻ",
    group: "services",
    component: "Services",
    name: "Services",
    icon: "satellite",
    scope: "LIST_SERVICE",
  },
  {
    title: "Gói Chụp",
    group: "combos",
    component: "combos",
    name: "Combos",
    icon: "photo_library",
    scope: "LIST_COMBO",
  },
  {
    title: "Đơn Hàng",
    name: "Order",
    component: "Order",
    icon: "reorder",
  },
  {
    title: "Công việc",
    group: "order-job",
    component: "OrderJob",
    name: "order-job",
    icon: "calendar_today",
     items: [
       { name: "Order-job", title: "Phân công", component: "order-job" },
       { name: "Assigned", title: "Danh sách", component: "Assigned" },
      ]
  },
  // {
  //   title: "Phân công công  việc",
  //   icon: "add_to_photos",
  //   name: "order-job"
  // },
  // {
  //   title: "Danh sách đặt lịch",
  //   name: "Assigned",
  //   group: "assigned",
  //   component: "Assigned",
  //   icon: "widgets",
  // },
  {
    title: "Chi phí phát sinh",
    icon: "monetization_on",
    name: "incurred",
    scope: "LIST_INCURRED"
  },
  {
    title: "Bảng lương",
    icon: "money",
    name: "salary"
    // scope: "LIST_CATEGORY"
  },
  /// caif ddatj luong
  {
    title: "Cài đặt lương",
    group: "config-salary",
    component: "ConfigSalary",
    name: "config-salary",
    icon: "calendar_today",
     items: [
       { name: "config_salary", title: "Nhân viên", component: "config_salary" },
       { name: "config_partner", title: "Cộng tác viên", component: "config_partner" },
      ]
  },

  ///
  {
    title: "Chi tiêu",
    name: "expense",
    component: "Expense",
    icon: "money",
    // scope: "LIST_EXPENSE"
  },
  {
    title: "Setting",
    group: "setting",
    name: "setting",
    component: "Setting",
    icon: "settings",
    items: [
        { name: "Photographer", title: "Photographer", component: "Photographer" },
        { name: "Photoshoper", title: "Photoshoper", component: "Photoshoper" },
        { name: "Saler", title: "Saler", component: "Saler" },
    ]

  },

  // {
  //   title: "Dịch vụ",
  //   group: "widgets",
  //   component: "widgets",
  //   icon: "widgets",
  //   items: [
  //     { name: "social", title: "Social", component: "SocialWidget" },
  //     {
  //       name: "StatisticWidget",
  //       title: "Statistic",
  //       badge: "new",
  //       component: "StatisticWidget"
  //     },
  //     { name: "chart", title: "Chart", component: "ChartWidget" },
  //     { name: "list", title: "List", component: "ListWidget" }
  //   ]
  // },
  // {
  //   title: "Đơn hàng",
  //   group: "widgets",
  //   component: "widgets",
  //   icon: "widgets",
  //   items: [
  //     { name: "social", title: "Social", component: "SocialWidget" },
  //     {
  //       name: "statistic",
  //       title: "Statistic",
  //       badge: "new",
  //       component: "StatisticWidget"
  //     },
  //     { name: "chart", title: "Chart", component: "ChartWidget" },
  //     { name: "list", title: "List", component: "ListWidget" }
  //   ]
  // },
  // { header: "CMS" },
  // {
  //   title: "List & Query",
  //   group: "layout",
  //   icon: "view_compact",
  //   items: [{ name: "Table", title: "Basic Table", component: "ListTable" }]
  // }
]
// reorder menu
Menu.forEach(item => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase()
      let textB = y.title.toUpperCase()
      return textA < textB ? -1 : textA > textB ? 1 : 0
    })
  }
})

export default Menu
