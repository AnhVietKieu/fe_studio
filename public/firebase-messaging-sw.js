// Give the service worker access to Firebase Messaging.
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyDXItL04TZaz8XXkznUjMwDwjtHgTtFFOk",
    authDomain: "logistic-dc8a9.firebaseapp.com",
    databaseURL: "https://logistic-dc8a9.firebaseio.com",
    projectId: "logistic-dc8a9",
    storageBucket: "",
    messagingSenderId: "269017212389",
    appId: "1:269017212389:web:0cea8393d23b5f5c"
};

firebase.initializeApp(config);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
